/**
 * Created by jsj on 27/05/2017.
 */
var main = {

    countJogador: function () {
        tools.ajax(url.countJogador, null, url.post, function (result) {
            $('#count_jogador').html(result);
        });
    },

    countEnquete: function () {
        tools.ajax(url.countEnquete, null, url.post, function (result) {
            $('#count_enquete').html(result);
        });
    },

    sumGolsByJogador: function (jogador) {
        var data = {'jogador': jogador};
        tools.ajax(url.sumGol, data, url.post, function (result) {
            $('#sum_gol').html(result);
        });
    },

    sumVitoriaByJogador: function (jogador) {
        var data = {'jogador': jogador};
        tools.ajax(url.sumVitoria, data, url.post, function (result) {
            $('#sum_vitorias').html(result);
        });
    },
    
    getJogadores: function (jogador) {
        var data = {'id': jogador};
        tools.ajax(url.getJogadores, data, url.post, function (result) {
            var json = JSON.parse(result);
            console.log(json);
        });
    },

    initTableRaxas: function (jogador) {
        $('#resultados_jogos').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": url.getRaxaByJogador+'?jogador='+1,
                "dataSrc": "raxa"
            },
            "columns": [
                { "data": "dia" },
                { "data": "gol"},
                { "data": "vitoria"},
                { "data": "id"}
            ],
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                },
            ]
        });
    }

};