<?php

/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/05/2017
 * Time: 23:08
 */

require ('../banco/DBRaxa.php');
require ('../model/Jogador.php');

class JogadorDAO {

    private $db;
    private $jogador;

    /**
     * JogadorDAO constructor.
     */
    public function __construct() {
        $this->db = new DBRaxa();
    }

    public function getAllJogadores () {

        try {
            $jogadores = array();
            $result = mysql_query("SELECT * FROM jogador ORDER BY nome") or die(mysql_error());
            if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $this->jogador = new Jogador();
                    $this->jogador->setId($row["id"]);
                    $this->jogador->setNome($row["nome"]);
                    $this->jogador->setFile($row["file"]);
                    array_push($jogadores, $this->jogador);
                }
                return $this->returnDefault(0, 'Dados retornados com sucesso!', $jogadores);
            }

        } catch (Exception $e) {
            return $this->returnDefault($e->getCode(), 'Erro ao consultar jogadores', null);
        }
    }

    protected function returnDefault ($code, $message, $data) {
        return array (
            'code' => $code,
            'message' => $message,
            'data' => $data
        );
    }
}